# imports
import json
import requests
import datetime

"""
Méthode d'insertion des données météorologiques actuelles'
"""


def insertWeatherSea(col):
    token = "dd3bfd5a58098deacda5b87b751a2ea0"  # Token de l'API

    """
        Coordonnées stratégiques en méditérannée
        """
    ports = open('/home/script/ports.json')
    coordinates = json.load(ports)
    coordinates = coordinates['data']

    for point in coordinates:  # Pour chaque coordonnées du tableau coordinates

        url = "http://api.openweathermap.org/data/2.5/weather?lat=" + str(point['lat']) + "&lon=" + str(
            point['long']) + "&units=metric&appid=" + token  # Url de l'api en fonction du point

        """
        Lecture des données au format JSON
        """
        request = requests.get(url)
        text = request.text
        data = json.loads(text)

        """
        Météo acutelle
        """
        weather = data['weather']
        weather = weather[0]
        weather = weather['description']

        """
        Données venteuses
        """
        wind = data['wind']
        wind_speed = wind['speed']
        wind_direction = wind['deg']

        """
        Température actuelle
        """
        main = data['main']
        temp = main['temp']

        """
        Formatage de l'envoi
        """
        data_send = {
            'longitude': point['long'],
            'latitude': point['lat'],
            'name': point['name'],
            'date': datetime.datetime.now(),
            'weather': weather,
            'wind_speed': wind_speed,
            'wind_direction': wind_direction,
            'temperature': temp
        }

        col.insert_one(data_send)  # Insertion des données dans la collection
