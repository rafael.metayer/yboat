from sqlalchemy import *
import insertBoatList


def insertSQL():
    """Connection à la base SQL"""
    mysql_conn_str = "mysql+pymysql://root:password-123@mysql_db:3306/YBoat"
    engine = create_engine(mysql_conn_str)
    connection = engine.connect()
    """Si la table boat_list n'existe pas"""
    if not engine.dialect.has_table(connection, 'boat_list'):
        connection.execute(
            '''CREATE TABLE boat_list  (id VARCHAR(100) PRIMARY KEY, name VARCHAR(100), category VARCHAR(30), country VARCHAR(30), sail VARCHAR(30), image VARCHAR(500));''')
        print("created")
    """Script d'insertion des données"""
    insertBoatList.insertBoatList(connection)
