from time import sleep

from pymongo import MongoClient

import insertWeatherNow
import insertWeather7Days
import insertBoatLocalisation


def InsertMongo():
    # Container MongoDB
    client = MongoClient('mongodb://root56:password123@mongo:27017/')

    # Base de données et collections
    db = client.Yboat
    colBoat = db.boatLocalisation
    colWeather7days = db.weather7Days
    colWeatherNow = db.weatherNow
    # Fonctions d'insertion
    insertBoatLocalisation.insertBoatLocalisation(colBoat)
    insertWeather7Days.insertWeather7Days(colWeather7days)
    insertWeatherNow.insertWeatherSea(colWeatherNow)
