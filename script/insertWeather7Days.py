# imports
import json
import requests

"""
Insertion des données météorologique sur 7 jours
"""


def insertWeather7Days(col):
    token = "dd3bfd5a58098deacda5b87b751a2ea0"  # Token de l'API

    """
    Coordonnées stratégiques en méditérannée
    """
    ports = open('/home/script/ports.json')
    coordinates = json.load(ports)
    coordinates = coordinates['data']

    col.delete_many({})  # Effacer tous les documents de la collection

    for point in coordinates:  # Pour chaque coordonnées du tableau coordinates
        url = "http://api.openweathermap.org/data/2.5/onecall?lat=" + str(point['lat']) + "&lon=" + str(point['lat']) \
              + "&units=metric&appid=" + token  # Url de l'api en fonction du point géographique
        """
        Lecture du JSON
        """
        request = requests.get(url)
        text = request.text
        data = json.loads(text)
        records = data['daily']

        for day in range(1, 7):  # Pour chaque jour prévisionnel
            weatherDay = records[day]  # Données du jour

            """
            Temps prévu
            """
            weather = weatherDay['weather']
            weatherDesc = weather[0]
            weatherDesc = weatherDesc['description']

            """
            Donénes venteuses
            """
            wind_speed = weatherDay['wind_speed']
            wind_direction = weatherDay['wind_deg']

            """
            Données sur la température prévue
            """
            temperature = weatherDay['temp']
            tempMax = temperature['max']
            tempMin = temperature['min']

            """
            Formatage de l'envoi
            """
            data_send = {
                'longitude': point['long'],
                'latitude': point['lat'],
                'name': point['name'],
                'predict_day': day,
                'weather': weatherDesc,
                'wind_speed': wind_speed,
                'wind_direction': wind_direction,
                'temperature_max': tempMax,
                'temperature_min': tempMin
            }

            col.insert_one(data_send)  # Insertion des données dans la collection
