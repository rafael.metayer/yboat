# imports
import json
import requests
import datetime


"""
Localisation des bateaux en méditerannée
"""


def insertBoatLocalisation(col):
    """
    Cadriage de la zone mediterranée et division de celle-ci mini-zones internes
    """
    minLat = 30
    maxLat = 45
    minLong = -5
    maxLong = 35
    for x in range(minLat, maxLat - 5, 5):
        x5 = x + 5
        for y in range(minLong, maxLong - 5, 5):
            y5 = y + 5
            if x != -5 and y != 45:
                url = "https://api.boataround.com/v1/search?coordinates=" + str(x) + "," + str(y5) + "," + str(
                    x5) + "," + str(y)  # URL de l'API
                token = "dd3bfd5a58098deacda5b87b751a2ea0"  # Token de l'API meteo
                """
                Lecture des données JSON
                """
                requestBat = requests.get(url)
                text = requestBat.text
                data = json.loads(text)

                """
                Stocker les résultats
                """
                records = data['data']
                records = records[0]
                if records['totalBoats'] != 0:
                    records = records['data']
                    for element in records:  # Pour chaque beteau
                        """
                        Coordonnées du bateau
                        """
                        coordonnees = element['coordinates']

                        """
                        Météo du bateau
                        """

                        url = "http://api.openweathermap.org/data/2.5/weather?lat=" + str(coordonnees[0]) + "&lon=" + str(
                            coordonnees[1]) + "&units=metric&appid=" + token  # Url de l'api en fonction du point
                        requestMet = requests.get(url)
                        text = requestMet.text
                        data = json.loads(text)

                        """
                        Météo acutelle
                        """
                        weather = data['weather']
                        weather = weather[0]
                        weather = weather['description']

                        """
                        Données venteuses
                        """
                        wind = data['wind']
                        wind_speed = wind['speed']
                        wind_direction = wind['deg']

                        """
                        Température actuelle
                        """
                        main = data['main']
                        temp = main['temp']

                        """
                        Formatage de l'envoi
                        """
                        data_send = {'id': element['_id'],
                                     'date': datetime.datetime.now(),
                                     'longitude': coordonnees[0],
                                     'latitude': coordonnees[1],
                                     'weather': weather,
                                     'wind_speed': wind_speed,
                                     'wind_direction': wind_direction,
                                     'temperature': temp
                                     }

                        """
                            Mise à jour du bateau
                        """
                        idBoat = {'id': data_send['id']}
                        col.delete_one(idBoat)
                        col.insert_one(data_send)
