# imports
import json

import requests

"""
Métode d'insertion de la liste des bateaux
"""


def insertBoatList(connection):
    """
        Cadriage de la zone mediterranée et division de celle-ci mini-zones internes
    """
    minLat = 30
    maxLat = 45
    minLong = -5
    maxLong = 35

    for x in range(minLat, maxLat - 5, 5):
        x5 = x + 5
        for y in range(minLong, maxLong - 5, 5):
            y5 = y + 5
            if x != -5 and y != 45:
                url = "https://api.boataround.com/v1/search?coordinates=" + str(x) + "," + str(y5) + "," + str(
                    x5) + "," + str(y)  # URL de l'API
                """
                Lecture des données au format JSON
                """
                request = requests.get(url)
                text = request.text
                data = json.loads(text)

                """
                Stocker les résultats
                """
                records = data['data']
                records = records[0]
                if records['totalBoats'] != 0:
                    records = records['data']
                    for element in records:  # Pour Chaque bateau
                        """
                        Recherche du bateau dans la base
                        """
                        data = {"id": element['_id']}
                        statement = """SELECT * FROM boat_list WHERE id=%(id)s"""
                        number_id = connection.execute(statement, data).fetchone()
                        if number_id is None:  # Si le bateau n'existe pas
                            """
                            Insertion du bateau dans la base
                            """
                            data = {"id": element['_id'], "name": element['title'], "category": element['category'],
                                    "country": element['country'], "sail": element['sail'], "image": element['thumb']}
                            statement = """INSERT INTO boat_list (id, name, category, country, sail, image) VALUES (%(id)s, %(name)s, %(category)s, %(country)s, %(sail)s, %(image)s)"""
                            connection.execute(statement, data)
