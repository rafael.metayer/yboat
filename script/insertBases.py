from time import sleep

import ScriptMongo
import ScriptSQL

while True:
    # Exécuter les scripts d'insertion
    ScriptSQL.insertSQL()
    ScriptMongo.InsertMongo()

    # Patienter 15 minutes
    print("waiting 15 min")
    sleep(900)
