# YBoat

## Commandes importantes

### Télécharger le projet

git clone 'https://gitlab.com/rafael.metayer/yboat.git'

### Lancer pour la 1ère fois l'application

A la racine du dossier, lancer les commandes suivantes dans le cmd :

Lancer pour la 1ère fois l'application :
docker-compose up -d

Afficher les container :
docker ps

Entrer dans le container python :
docker exec -it yboat_python_1 bash

Installer les librairie :
pip install sqlalchemy pymysql pymongo requests

Lancer le script d'insertion des données :
python /home/script/insertBases.py

Sortir du container :
exit

Entrer dans le container python :
docker exec -it yboat_flask_1 bash

Installer les librairie :
pip install sqlalchemy pymysql pymongo flask

Lancer le script d'insertion des données :
python /home/front/app.py

Sortir du container :
exit

Accéder au site web :
http://localhost:5000/

### Relancer un container déjà existant

A la racine du dossier, lancer les commandes suivantes dans le cmd :

Lancer l'application :
docker-compose up -d

Afficher les container :
docker ps

Entrer dans le container python :
docker exec -it yboat_python_1 bash

Lancer le script d'insertion des données :
python /home/script/insertBases.py

Sortir du container :
exit

Entrer dans le container python :
docker exec -it yboat_flask_1 bash

Lancer le script d'insertion des données :
python /home/front/app.py

Sortir du container :
exit

Accéder au site web :
http://localhost:5000/

Pour éteindre l'application :
docker-compose stop

### Réinitialiser l'application

A la racine du dossier, lancer les commandes suivantes dans le cmd :

Eteindre l'application :
docker-compose stop

Supprimer l'application :
docker-compose rm -f

Pour installer les container :
docker-compose up -d

Afficher les container :
docker ps

Entrer dans le container python :
docker exec -it yboat_python_1 bash

Installer les librairie :
pip install sqlalchemy pymysql pymongo requests

Lancer le script d'insertion des données :
python /home/script/insertBases.py

Sortir du container :
exit

Entrer dans le container python :
docker exec -it yboat_flask_1 bash

Installer les librairie :
pip install sqlalchemy pymysql pymongo flask

Lancer le script d'insertion des données :
python /home/front/app.py

Sortir du container :
exit

Accéder au site web :
http://localhost:5000/
