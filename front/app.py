from datetime import datetime, timedelta

from flask import Flask, render_template, url_for
from pymongo import MongoClient
from sqlalchemy import *
from json import dumps

"""
Base de données mongo et collections
"""
client = MongoClient('mongodb://root56:password123@mongo:27017/')
db = client.Yboat
colBoat = db.boatLocalisation
colWeather7days = db.weather7Days
colWeatherNow = db.weatherNow

"""
Base de données SQL et table
"""
mysql_conn_str = "mysql+pymysql://root:password-123@mysql_db:3306/YBoat"
engine = create_engine(mysql_conn_str)
connection = engine.connect()

"""
Application Flask
"""
app = Flask(__name__, template_folder="templates")


"""
Route index liée à l'adresse '/'
Sert de dashboard, y sont affiché une carte, la liste des bateaux dans le monde, et la liste des points stratégiques météos
"""
@app.route('/')
def index():
    """selection de toute la liste des bateaux"""
    result = connection.execute("""SELECT * FROM boat_list""")
    boats = []
    for row in result:
        boats.append(
            {'id': row[0], 'name': row[1], 'category': row[2], 'country': row[3], 'sail': row[4], 'image': row[5]})

    """Calcul des 15 dernières minutes"""
    date15min = datetime.now() - timedelta(hours=0, minutes=15)
    queryDate = {"date": {"$gt": date15min}}

    """Récupération des bateaux et points météo de moins de 15 min"""
    boatLocalisation = colBoat.find(queryDate)
    weathernow = colWeatherNow.find(queryDate)
    weatherList = colWeatherNow.find(queryDate)

    """Association de la liste des bateaux avec les position des bateaux de moins de 15 min"""
    boatsLocalisationName = []
    for boat in boatLocalisation:
        boatSQL = list(filter(lambda x: x["id"] == boat['id'], boats))
        if boatSQL is not None:
            boatName = boatSQL[0]['name']
        else:
            boatName = 'Undifined'
        boatsLocalisationName.append({'id': boat['id'],
                                      'lat': boat['latitude'],
                                      'long': boat['longitude'],
                                      'name': boatName,
                                      'weather': boat['weather']})

    """Envoyer les requêtes à l'html associé"""
    return render_template('index.html', boatLocalisation=boatsLocalisationName, weathernow=weathernow,
                           boatlist=boats, weatherList=weatherList)


"""La route pour afficher les infos d'un bateau"""
@app.route('/boat/<id_boat>')
def info_boat(id_boat):
    """Selectionner le bateaux associé à l'id dans le lien retrouvé dans la base SQL"""
    statement = """SELECT * FROM boat_list WHERE id=%(id_boat)s"""
    boat = connection.execute(statement, id_boat=id_boat).fetchone()
    boats = []
    for row in boat:
        boats.append(
            row)

    """Retrouver la dernière position associé à l'id dans le lien"""
    boatLocalisation = colBoat.find_one({"id": id_boat})

    """Envoyer les requêtes à l'html associé"""
    return render_template('boat.html', boat=boats, boatLocalisation=boatLocalisation)


"""La route pour afficher les infos d'un point météo"""
@app.route('/weather/<latitude>/<longitude>')
def info_weather(latitude, longitude):
    """Float de la longitude et la latitude envoyées dans le lien"""
    latitude = float(latitude)
    longitude = float(longitude)

    """Historique météo du point recherché"""
    weatherlast = colWeatherNow.find({"latitude": latitude, "longitude": longitude}).sort("date", -1)
    """Prédiction météo du point recherché"""
    weather7days = colWeather7days.find({"latitude": latitude, "longitude": longitude}).sort("predict_day")
    """météo de moins de 15 min du point recherché"""
    date15min = datetime.now() - timedelta(hours=0, minutes=15)
    queryDate = {"date": {"$gt": date15min}, "latitude": latitude, "longitude": longitude}
    weathernow = colWeatherNow.find_one(queryDate)

    """Envoyer les requêtes à l'html associé"""
    return render_template('meteo.html', weatherlast=weatherlast, weather7days=weather7days, weathernow=weathernow)


"""Application web"""
if __name__ == '__main__':
    app.run(host="0.0.0.0", port=5000, debug=True)
